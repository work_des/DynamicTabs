﻿$(function () {
    i = 1;

    $('.b-tabs').on('click', '.b-tab-header__title', function (e) {

        var $this = $(this).parent();

        $this.parent('.b-tabs__item').toggleClass('aktiv');

        $this.next('.b-tab-content').fadeToggle(300);

        $this.parent('.b-tabs__item').siblings('.b-tabs__item').removeClass('aktiv').find('.b-tab-content').hide();

        e.stopPropagation();
    });

    $('.b-tabs').on('click', '.b-tab-header__close', function (e) {

        var $this = $(this).parent();

        $this.parent('.b-tabs__item').remove();

        e.stopPropagation();
    });
});

function TabCreate() {
    var data = {
        title: 'Вкладка №' + i++,
        body: '<b>Основная идея БЭМ-методологии</b> — сделать разработку простой и быстрой, а работу в команде — понятной и слаженной.'
    };

    $("#tmpl_tab").tmpl(data).insertBefore(".b-tabs__create");
}